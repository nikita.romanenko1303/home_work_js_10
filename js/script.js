const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItem = document.querySelectorAll(".tabs-item");
tabsTitle.forEach(function (item) {
    item.addEventListener("click", function () {
        let currentTabs = item;
        let tabId = currentTabs.getAttribute("data-tab");
        let currentTabID = document.querySelector(tabId);
        tabsTitle.forEach(function (item) {
            item.classList.remove("active");
        })

        tabsItem.forEach(function (item) {
            item.classList.remove("active");
        })
        currentTabs.classList.add("active");
        currentTabID.classList.add("active");
    })
})